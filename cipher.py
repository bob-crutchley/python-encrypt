from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

def generate_key_pair():
        private_key = rsa.generate_private_key(
                public_exponent=65537,
                key_size=2048,
                backend=default_backend()
        )
        public_key = private_key.public_key()
        return {
                "public": serialize_public_key(public_key),
                "private": serialize_private_key(private_key)
        }

def serialize_private_key(private_key):
        return private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.PKCS8,
                encryption_algorithm=serialization.NoEncryption()
        )
        
def serialize_public_key(public_key):
        return public_key.public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
        )

def deserialize_public_key(serialized_public_key):
        return serialization.load_pem_public_key(
                serialized_public_key,
                backend=default_backend()
        )

def deserialize_private_key(serialized_private_key):
        return serialization.load_pem_private_key(
                serialized_private_key,
                password=None,
                backend=default_backend()
        )

def encrypt(message, public_key):
        return public_key.encrypt(
                bytes(message.encode("utf-8")),
                padding.OAEP(
                        mgf=padding.MGF1(algorithm=hashes.SHA256()),
                        algorithm=hashes.SHA256(),
                        label=None
                )
        )

def decrypt(encrypted, private_key):
        return private_key.decrypt(
                encrypted,
                padding.OAEP(
                        mgf=padding.MGF1(algorithm=hashes.SHA256()),
                        algorithm=hashes.SHA256(),
                        label=None
                )
        ).decode("utf-8")

