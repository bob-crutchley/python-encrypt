import requests
import cipher

keys = cipher.generate_key_pair()
request = requests.post("http://localhost:5000/key-exchange", keys["public"])
print(request.text.encode("utf-8"))
server_public_key = cipher.deserialize_public_key(request.text.encode("utf-8"))

message = "Hello from client"
encrypted_message = cipher.encrypt(message, server_public_key)
request = requests.post("http://localhost:5000/", encrypted_message)
print(request.text)
