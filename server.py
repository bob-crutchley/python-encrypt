from flask import Flask
from flask import request
import cipher

app = Flask(__name__)

keys = cipher.generate_key_pair()

client_key = None

@app.route("/key-exchange", methods = ['POST'])
def key_exchange():
    print(request.data)
    client_key = cipher.deserialize_public_key(request.data)
    return keys["public"]

@app.route("/", methods = ['POST'])
def default():
    print(cipher.decrypt(request.data, cipher.deserialize_private_key(keys["private"])))
    return "Hello from server"

app.run()
