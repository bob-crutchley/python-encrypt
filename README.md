# Python Encrypt
Simple client server application that uses asymmetric (RSA) encryption.

## Setting up
Make sure you have Python 3 installed

### Create a virtual environment for the project
```bash
virtualenv venv
```
### Activate the virtual environment
```bash
# For Linux
source venv/bin/activate
```
```bat
:: For Windows 
venv\Scripts\activate
```
### Install pip dependencies
```bash
pip install -r dependencies.txt
```
### Run the application
First execute the `server.py` script, and then run the `client.py` script
